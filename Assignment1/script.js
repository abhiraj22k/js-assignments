const form = document.getElementById("form");
form.addEventListener('submit', handleFormSubmit);
function handleFormSubmit(e) {
    const formObj = {};
    e.preventDefault();
    const domForm = e.target;
    formObj.email = domForm.email.value;
    formObj.password = domForm.password.value;
    formObj.sex = domForm.sex.value;
    formObj.role = getRadioValue(domForm.role);
    formObj.permissions = getCheckboxValues(domForm.permission);
    isValid(formObj, result);
}
function result(res, type, formObj = {}) {
    const errorDiv = document.querySelector(".error-message")
    switch (type) {
        case "error":
            errorDiv.style.color = "#ff0000";
            errorDiv.innerText = res;
            return;
        case "success":
            form.style.display = 'none';
            errorDiv.innerHTML = getConfirmHTML(formObj);
            errorDiv.style.color = "#143a56"
            return;
    }
}
function getConfirmHTML(formObj) {
    let permissions = "";
    formObj.permissions.forEach(obj => {
        permissions += `<span>${obj}</span><span>, </span>`;
    })
    const confirmHTML = `<div>Email: ${formObj.email}</div><div>password: ${formObj.password}</div><div>sex:${formObj.sex}</div><div>Role: ${formObj.role}</div><div class='permission' >Permissions: ${permissions}</div><button>Confirm</button>`;
    return confirmHTML;
}
function getRadioValue(radioArr) {
    for (let i = 0; i < radioArr.length; i++) {
        if (radioArr[i].checked) {
            return radioArr.value;
        }
    }
    return "";
}
function getCheckboxValues(checkArr) {
    const tempArr = [];
    for (let i = 0; i < checkArr.length; i++) {
        if (checkArr[i].checked) {
            tempArr.push(checkArr[i].value);
        }
    }
    return tempArr.length == 0 ? "" : tempArr;
}
function isEmailValid(email) {
    const validator = /^\w([\.]?\w+)*\@\w+([\.]?\w+)*(\.\w{2,3})+$/;
    if (email.match(validator)) return true;
    else return false
}
function isValid(formObj, result) {
    for (let value of Object.values(formObj)) {
        if (value == "") {
            return result("Please fill all fields", "error");
        }
    }
    if (!isEmailValid(formObj.email)) {
        return result("Incorrect email format", "error");
    }
    if (formObj.permissions.length < 2) {
        return result("Please allow atleast 2 permissions", "error");
    }
    return result("Successful", "success", formObj);
}