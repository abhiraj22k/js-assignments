class Person {
    constructor(name, age, salary, sex) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }
    static sort(arr, field, order) {
        const tempArr = arr;
        this.#quickSort(tempArr, field, order, 0, tempArr.length - 1);
        return tempArr;
    }
    // Quick sort implementation (Slightly changed)
    static #quickSort(arr, field, order, start, end) {
        if (start > end) {
            return;
        }
        const temp = this.#createPartition(arr, field, start, end, order);
        this.#quickSort(arr, field, order, start, temp - 1);
        this.#quickSort(arr, field, order, temp + 1, end);
    }
    static #createPartition(arr, field, start, end, order) {
        const pivot = arr[end][field];
        let left = start - 1;
        for (let i = start; i <= end - 1; i++) {
            if (order == 'asc') {
                if (arr[i][field] < pivot) {
                    left++;
                    this.#swap(arr, left, i);
                }
            } else if (order == 'desc') {
                if (arr[i][field] > pivot) {
                    left++;
                    this.#swap(arr, left, i);
                }
            }
        }
        this.#swap(arr, left + 1, end);
        return left + 1;
    }
    static #swap(arr, i, j) {
        const temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
const person1 = new Person("abhishek", 21, 100, "male");
const person2 = new Person("roshani", 24, 500, "female");
const person3 = new Person("aditya", 21, 50, "male");
const person4 = new Person("raghav", 22, 200, "male");
const person5 = new Person("pranav", 20, 150, "male");
const person6 = new Person("aditi", 22, 150, "female");
const personsArray = [person1, person2, person3, person4, person5, person6];

const ansArr = Person.sort(personsArray, 'name', 'asc'); // use Static method sort() to sort the Persons array
console.log(ansArr); // Check Console for results